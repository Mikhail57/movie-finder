import os
from pymediainfo import MediaInfo

videos_directory = 'C:\\Users\\misha\\Downloads\\videos'
video_extensions = ('mp4', 'mkv', 'avi')
unacceptable_codecs = ['aac']


# traverse root directory, and list directories as dirs and files as files
for root, dirs, files in os.walk(videos_directory):
    path = root.split(os.sep)
    # print((len(path) - 1) * '---', os.path.basename(root))
    for dir in dirs:
        for folder_root, _, video_files in os.walk(os.path.join(root, dir)):
            breaked = False
            for video_file in video_files:
                if video_file.split('.')[-1].lower() not in video_extensions:
                    continue
                media_info = MediaInfo.parse(str(os.path.join(folder_root, video_file)))
                for track in media_info.tracks:
                    # print(track.to_data())
                    if track.track_type != 'Audio':
                        continue
                    if track.format.lower() in unacceptable_codecs:
                        print(f'Found uncompatable codec {track.format} in folder {folder_root} in file {video_file}')
                        breaked = True
                        break
                if breaked:
                    break
            if breaked:
                break
